#FROM alpine:3.12
FROM debian:stable

#RUN echo “http://dl-cdn.alpinelinux.org/alpine/edge/community” >> /etc/apk/repositories
#RUN apk update && apk add --no-cache libstdc++ libc6-compat libnsl

RUN apt update && apt dist-upgrade -y
RUN apt install -y openssl

#RUN ls -s /lib/libc.musl-x86_64.so.1 /lib/ld-linux-x86_64.so2
#RUN ln -s /usr/lib/libnsl.so.2 /usr/lib/libnsl.so.1

WORKDIR /opt
COPY ./bin .
EXPOSE 6012
CMD ["./DozeChat"]
