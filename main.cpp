#include <iostream>
#include "include/tcp_server.h"
#include "include/chatroom.h"
#include "include/api.h"
#include "include/grpc_server.h"

using namespace std;

int main()
{
		//bdd.connexion_database();
	try
	{
		boost::asio::io_context ioc;
//		ssl::context ctx{ssl::context::sslv23};
		// Création d'un serveur
		auto const doc_root = std::make_shared<std::string>("/");
		tcp::endpoint endpoint(tcp::v4(), 4000);
		tcp::endpoint apiEndpoint(tcp::v4(), 6012);

		std::make_shared<tcp_server>(ioc, endpoint)->run();

//		tcp_server server(ioc, endpoint);
 		listener api(ioc, apiEndpoint, doc_root);
		grpc_server grpcSDK;
//		api.run();
/*        std::thread server_thread([&]
        {
            grpcSDK.run();
        });*/
        std::thread http_thread([&]
        {
            ioc.run();
        });
        //server_thread.join();
        http_thread.join();

	}
	catch (std::exception& e)
	{
		std::cerr << e.what() << std::endl;
	}

	return 0;
}
