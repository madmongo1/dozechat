#ifndef WEBSOCKET_SESSION_H
#define WEBSOCKET_SESSION_H

#include <boost/enable_shared_from_this.hpp>
#include <boost/shared_ptr.hpp>

#include <boost/asio.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/asio/bind_executor.hpp>
#include <boost/asio/strand.hpp>
#include <boost/asio/ip/tcp.hpp>

#include <deque>
#include "logger.h"
#include "message.h"
#include "chatroom.h"

namespace beast = boost::beast;
namespace net = boost::asio;
namespace http = beast::http;
namespace websocket = boost::beast::websocket;
using tcp = boost::asio::ip::tcp;

class chatroom;
class websocket_session : public boost::asio::coroutine, public boost::enable_shared_from_this<websocket_session>
{
    std::vector<std::shared_ptr<boost::beast::multi_buffer>> chat_message_queue ;
public:
    websocket_session(tcp::socket&& socket, chatroom &room);
    ~websocket_session();
    void run();
    void do_read(boost::beast::multi_buffer &buffer);
    void do_write(std::shared_ptr<boost::beast::multi_buffer> const& mess);

private:
    websocket::stream<boost::beast::tcp_stream> ws;
    boost::beast::multi_buffer buffer;
    void connect(beast::error_code ec, std::size_t bytes_transferred);
    chatroom& m_room;
    logger log;
    };
#endif
