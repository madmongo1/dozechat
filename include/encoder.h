#ifndef ENCODER_H
#define ENCODER_H

#include <bzlib.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <iostream>

//extern "C"{
    #include <bsdiff.h>
//}

class encoder
{
    public:
        encoder();
        virtual ~encoder();
        void createPatch(const char* SourcePath, const char* TargetPath, const char* PatchPath);

    private:
        int fd;
        int bz2err;
        uint8_t *source,*target;
        uint8_t buf[8];
        off_t sourcesize,targetsize;
        BZFILE* bz2;
        FILE * pf;
	struct bsdiff_stream stream;
};

#endif // ENCODER_H
