#include <iostream>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/asio/coroutine.hpp>
#include <boost/asio/strand.hpp>
#include <memory>
#include "chatroom.h"
#include "websocket_session.h"
#include "tcp_session.h"
#include "logger.h"
#include <boost/beast/core.hpp>

namespace beast = boost::beast;
namespace net = boost::asio;
using tcp = boost::asio::ip::tcp;

class tcp_server : public boost::asio::coroutine, public std::enable_shared_from_this<tcp_server>
{
    tcp::acceptor acceptor;
    tcp::socket socket;
    net::io_context& ioc;
public:
	tcp_server(boost::asio::io_context& ioc, tcp::endpoint& endpoint);
	void do_accept();
	chatroom room;
	void run();
private:
	logger log;
	void loop(beast::error_code ec = {});
};
