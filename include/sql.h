#ifndef SQL_H
#define SQL_H

#include <mysql.h>
#include <iostream>
#include <cstring>

class sql
{
    public:
        sql();
        virtual ~sql();
        void connexion_database();
        void ajouter_entree();

    protected:

    private:
    void creer_database_table();

    MYSQL *database;
    char *user, *password, *bdd, *socket_path, *hote, *requete_bdd, *requete_table ;
    const char *infos_erreur;
    unsigned int port;
//    unsigned long flags;
};

#endif // SQL_H
