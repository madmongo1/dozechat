#ifndef MESSAGE_H
#define MESSAGE_H

#include <cstdio>
#include <cstdlib>
#include <cstring>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

namespace pt = boost::property_tree;

class message
{
public:

/*	void parse(message)
	{
	pt::read_json(message, root);
	int event = root.get<int>("event");
	std::cout << "event" << event << std::endl;
	}
*/
	enum { header_length = 4 };
  	enum { max_body_length = 512 };

message() : body_length(0)
{
}

const char* message_data() const
{
	return data;
}

char* message_data()
{
    return data;
}

std::size_t message_length() const
{
    return header_length + body_length;
}

const char* body() const
{
    return data + header_length;
}

char* body()
{
    return data + header_length;
}

std::size_t message_body_length() const
{
    return body_length;
}

void chat_body_length(std::size_t new_length)
{
    body_length = new_length;
    if (body_length > max_body_length)
      body_length = max_body_length;
}

bool decode_header()
{
    char header[header_length + 1] = "";
    std::strncat(header, data, header_length);
    body_length = std::atoi(header);
    if (body_length > max_body_length)
    {
      body_length = 0;
      return false;
    }
    return true;
}

void encode_header()
{
    char header[header_length + 1] = "";
    std::sprintf(header, "%4d", static_cast<int>(body_length));
    std::memcpy(data, header, header_length);
}

void cleara()
{
    std::memcpy(data, "", header_length);
}

private:
	pt::ptree root;
	char data[header_length + max_body_length];
  	std::size_t body_length;
};

#endif
