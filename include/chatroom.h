#ifndef CHATROOM_H
#define CHATROOM_H

#include <boost/weak_ptr.hpp>
#include <boost/shared_ptr.hpp>
#include <iostream>
#include <set>
#include <deque>
#include <boost/beast/core.hpp>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include "message.h"
#include "websocket_session.h"
#include "logger.h"

namespace pt = boost::property_tree;
class websocket_session;

typedef std::deque<message> chat_message_queue;

class chat_participant
{
public:
  virtual ~chat_participant() {}
  virtual void deliver(const message& msg) = 0;
};

typedef std::shared_ptr<chat_participant> chat_participant_ptr;

class chatroom
{
public:
	chatroom();
	void join(chat_participant_ptr participant);
	void leave(chat_participant_ptr participant);
	void deliver(const message& msg);

private:
	std::set<chat_participant_ptr> participants;
    chat_message_queue recent_msgs;
    enum { max_recent_msgs = 100 };
	void parse(boost::beast::multi_buffer mess, int &event );
	pt::ptree root;
	logger log;
};

#endif // CHATROOM_H
