#ifndef JSON_PARSER_H
#define JSON_PARSER_H


#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

namespace pt = boost::property_tree;

class json_parser
{
    public:
        json_parser();
        virtual ~json_parser();
        float GetLatestVersion();

    private:
        pt::ptree root;
};

#endif // JSON_PARSER_H
