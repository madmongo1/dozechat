#include "chatroom.h"
#include "websocket_session.h"

chatroom::chatroom()
{
	log.info("New room");
}

void chatroom::join(chat_participant_ptr participant)
{
	participants.insert(participant);
	for (auto msg: recent_msgs)
      participant->deliver(msg);
}

void chatroom::leave(chat_participant_ptr participant)
{
	participants.erase(participant);
}

void chatroom::deliver(const message& msg)
{
    recent_msgs.push_back(msg);
    while (recent_msgs.size() > max_recent_msgs)
      recent_msgs.pop_front();

    for (auto participant: participants)
      participant->deliver(msg);
}

void chatroom::parse(boost::beast::multi_buffer mess, int &event )
{
        std::stringstream ss;
	ss << boost::beast::buffers_to_string(mess.data());
        pt::read_json(ss, root);
        event = root.get<int>("event");
}
