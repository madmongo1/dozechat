#include "tcp_session.h"

tcp_session::tcp_session(tcp::socket socket, chatroom& room)
    : socket(std::move(socket)),
      room(room)
{
}

void tcp_session::start()
{
    room.join(shared_from_this());
    do_read_header();
    message msg;
    char welcome_message[] = "Welcome on the Doze Chat ";
    msg.chat_body_length(std::strlen(welcome_message));
    std::memcpy(msg.body(), welcome_message, msg.message_body_length());
    msg.encode_header();
    deliver(msg);
}

void tcp_session::deliver(const message& msg)
{
    bool write_in_progress = !write_msgs.empty();
    write_msgs.push_back(msg);
    if (!write_in_progress)
    {
      do_write();
    }
  }

void tcp_session::do_read_header()
{
    auto self(shared_from_this());
    boost::asio::async_read(socket,
        boost::asio::buffer(read_msg.message_data(), message::header_length),
        [this, self](boost::system::error_code ec, std::size_t /*length*/)
        {
            do_read_body();
          if (!ec && read_msg.decode_header())
          {
            do_read_body();
          }
          else
          {
            room.leave(shared_from_this());
          }
        });
}

void tcp_session::do_read_body()
{
    auto self(shared_from_this());
    boost::asio::async_read(socket,
        boost::asio::buffer(read_msg.body(), read_msg.message_body_length()),
        [this, self](boost::system::error_code ec, std::size_t /*length*/)
        {
          if (!ec)
          {
            std::cout<<"message : "<<read_msg.body()<<std::endl;
            room.deliver(read_msg);
            do_read_header();
          }
          else
          {
                std::cout<<ec.message()<<std::endl;
            room.leave(shared_from_this());
          }
        });
}

void tcp_session::do_write()
{
    auto self(shared_from_this());
    boost::asio::async_write(socket,
        boost::asio::buffer(write_msgs.front().message_data(),
          write_msgs.front().message_length()),
        [this, self](boost::system::error_code ec, std::size_t /*length*/)
        {
          if (!ec)
          {
            write_msgs.pop_front();
            if (!write_msgs.empty())
            {
              self->do_write();
            }
          }
          else
          {
            room.leave(shared_from_this());
          }
        });
}
