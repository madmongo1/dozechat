#include "websocket_session.h"
#include <iostream>

websocket_session::websocket_session(tcp::socket&& socket, chatroom &room)
        : ws(std::move(socket))
	, m_room(room)
{
	log.info("Session created !");
}

websocket_session::~websocket_session()
{
	log.info("Session destroyed !");
}

void websocket_session::run()
{
/*	auto self(shared_from_this());
        // Accept the websocket handshake
        m_ws.async_accept(
	[self](boost::system::error_code ec)
	{
	  if (!ec)
	  {
		std::cout <<"Connection accepté"<< std::endl;
		//TODO: Create network or check if the user have already a or multiples network
//		self->m_room.join(self);
		self->do_read(self->buffer);
	  }
	  else
		return self->fail(ec, "accept");
       });*/
        net::dispatch(ws.get_executor(),
                      beast::bind_front_handler(&websocket_session::connect,
                                                shared_from_this(),
                                                beast::error_code{},
                                                0));
}

#include <boost/asio/yield.hpp>

 void websocket_session::connect( beast::error_code ec, std::size_t bytes_transferred)
 {
        boost::ignore_unused(bytes_transferred);
        reenter(*this)
        {
            // Set suggested timeout settings for the websocket
            ws.set_option(
                websocket::stream_base::timeout::suggested(
                    beast::role_type::server));

            // Set a decorator to change the Server of the handshake
            ws.set_option(websocket::stream_base::decorator(
                [](websocket::response_type& res)
                {
                    res.set(http::field::server,
                        std::string(BOOST_BEAST_VERSION_STRING) +
                            " websocket-server-stackless");
                }));

	    yield ws.async_accept(
                std::bind(
                    &websocket_session::connect,
                    shared_from_this(),
                    std::placeholders::_1,
                    0));
            if(ec)
                log.fail(ec, "accept");

	    for(;;)
            {
                // Read a message into our buffer
                yield ws.async_read(
                    buffer,
                    std::bind(
                        &websocket_session::connect,
                        shared_from_this(),
                        std::placeholders::_1,
                        std::placeholders::_2));
                if(ec == websocket::error::closed)
                {
                    // This indicates that the session was closed
                    return;
                }
                if(ec)
                    log.fail(ec, "read");

		ws.text(ws.got_text());
                yield ws.async_write(
                    buffer.data(),
                    std::bind(
                        &websocket_session::connect,
                        shared_from_this(),
                        std::placeholders::_1,
                        std::placeholders::_2));
                if(ec)
                    log.fail(ec, "write");

                // Clear the buffer
                buffer.consume(buffer.size());
	    }
	}
}

#include <boost/asio/unyield.hpp>

void websocket_session::do_read(boost::beast::multi_buffer &buffer)
{
        auto self(shared_from_this());
        ws.async_read(buffer,
                [self, &buffer](boost::beast::error_code ec, std::size_t /*length*/)
                {
			if(ec == boost::beast::websocket::error::closed)
			{
				//TODO: indicate user deconnection on all users
			}
			else
			{
                        	if (!ec)
                        	{
//				 self->m_room.deliver(buffer);
    				 // Clear the buffer
				 self->buffer.consume(buffer.size());
 				 self->do_read(self->buffer);
				}
                        	else
                               	std::cout<<"read : " << ec.message()<<std::endl;
                	}
		});
}

void websocket_session::do_write(std::shared_ptr<boost::beast::multi_buffer> const& mess )
{
	auto self(shared_from_this());
	chat_message_queue.push_back(mess);
	if(chat_message_queue.size() > 1)
        return;

	ws.async_write(
        chat_message_queue.front()->data(),
        [self](boost::system::error_code ec, std::size_t bytes)
        {
	if(!ec)
        	self->chat_message_queue.erase(self->chat_message_queue.begin());
	else
		std::cout<<"write : " << ec.message()<<std::endl;
        });
}
