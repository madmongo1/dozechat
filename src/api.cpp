#include "api.h"

api::api(
        tcp::socket&& socket,
        std::shared_ptr<std::string const> const& doc_root)
        : stream(std::move(socket))
        , doc_root(doc_root)
        , lambda(*this)
{
	net = new network ;
}

void api::run()
{
      do_read();
}

void api::verify_token(std::string token)
{
	std::string test = "eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwidXNlcm5hbWUiOiJ0ZXN0IiwiZW1haWwiOiIiLCJwYXNzd29yZCI6IiRhcmdvbjJpZCR2PTE5JG09NjU1MzYsdD0yLHA9MSR1N3RGYmpJWmpoYlg4VFo1L25pMk1BJDJ4azQ1NWo1UnYxK3RCTlZaTlB5QjIwenNzQjZoR1QwUE5QVmRQSHU1WFkiLCJwZXJtaXNzaW9uIjoxLCJpc0FjdGl2ZSI6MH0.Job3OuS2Hcy-uM6jZ8dkSsRmVV3LL39_gxDgrSH5NG4" ;
	std::cout<<token<<std::endl;
/*	auto decoded_token = jwt::decode(test, algorithms({"hs256"}), ec, secret(key), verify(true));
	switch(ec.value())
	{
	case static_cast<int>(jwt::VerificationErrc::InvalidAlgorithm):
		std::cout<<"error"<<std::endl;
	break;
	case static_cast<int>(jwt::VerificationErrc::TokenExpired):
		std::cout<<"error1"<<std::endl;
	break;
	case static_cast<int>(jwt::VerificationErrc::InvalidIssuer):
		std::cout<<"error2"<<std::endl;
	break;
	case static_cast<int>(jwt::VerificationErrc::InvalidSubject):
		std::cout<<"error3"<<std::endl;
	break;
	case static_cast<int>(jwt::VerificationErrc::InvalidIAT):
		std::cout<<"error4"<<std::endl;
	break;
	case static_cast<int>(jwt::VerificationErrc::InvalidJTI):
		std::cout<<"error5"<<std::endl;
	break;
	case static_cast<int>(jwt::VerificationErrc::InvalidAudience):
		std::cout<<"error6"<<std::endl;
	break;
	case static_cast<int>(jwt::VerificationErrc::ImmatureSignature):
		std::cout<<"error7"<<std::endl;
	break;
	case static_cast<int>(jwt::VerificationErrc::InvalidSignature):
		std::cout<<"error8"<<std::endl;
	break;
	case static_cast<int>(jwt::VerificationErrc::TypeConversionError):
		std::cout<<"error9"<<std::endl;
	break;
//	default:
	}
	std::cout << "issue : " << ec.message() << std::endl;
	std::cout << decoded_token.header() << std::endl;
	std::cout << decoded_token.payload() << std::endl;*/
}

void api::do_read()
{
        // Make the request empty before reading,
        // otherwise the operation behavior is undefined.
        req = {};

        // Read a request
        http::async_read(stream, buffer, req,
            boost::beast::bind_front_handler(
                    &api::on_read,
                    shared_from_this()));
}

void api::on_read(boost::beast::error_code ec, std::size_t bytes_transferred)
{
        boost::ignore_unused(bytes_transferred);

        // This means they closed the connection
        if(ec == http::error::end_of_stream)
            return do_close();

        if(ec)
	    std::cout<< "read : "<< ec.message()<<std::endl;

	 // Send the response
        handle_request(*doc_root, std::move(req), lambda);
}

void api::on_write(bool close, boost::beast::error_code ec, std::size_t bytes_transferred)
{
        boost::ignore_unused(bytes_transferred);

        if(ec)
            //return fail(ec, "write");
	    std::cout<< "write :"<< ec.message()<<std::endl;

	if(close)
        {
            // This means we should close the connection, usually because
            // the response indicated the "Connection: close" semantic.
            return do_close();
        }

        // We're done with the response so delete it
        res = nullptr;

        // Read another request
        do_read();
}

void api::do_close()
{
        // Send a TCP shutdown
        boost::beast::error_code ec;
        stream.socket().shutdown(tcp::socket::shutdown_send, ec);

        // At this point the connection is closed gracefully
}

boost::beast::string_view api::mime_type(boost::beast::string_view path)
{
    using boost::beast::iequals;
    auto const ext = [&path]
    {
        auto const pos = path.rfind(".");
        if(pos == boost::beast::string_view::npos)
            return boost::beast::string_view{};
        return path.substr(pos);
    }();
    if(iequals(ext, ".htm"))  return "text/html";
    if(iequals(ext, ".html")) return "text/html";
    if(iequals(ext, ".php"))  return "text/html";
    if(iequals(ext, ".css"))  return "text/css";
    if(iequals(ext, ".txt"))  return "text/plain";
    if(iequals(ext, ".js"))   return "application/javascript";
    if(iequals(ext, ".json")) return "application/json";
    if(iequals(ext, ".xml"))  return "application/xml";
    if(iequals(ext, ".swf"))  return "application/x-shockwave-flash";
    if(iequals(ext, ".flv"))  return "video/x-flv";
    if(iequals(ext, ".png"))  return "image/png";
    if(iequals(ext, ".jpe"))  return "image/jpeg";
    if(iequals(ext, ".jpeg")) return "image/jpeg";
    if(iequals(ext, ".jpg"))  return "image/jpeg";
    if(iequals(ext, ".gif"))  return "image/gif";
    if(iequals(ext, ".bmp"))  return "image/bmp";
    if(iequals(ext, ".ico"))  return "image/vnd.microsoft.icon";
    if(iequals(ext, ".tiff")) return "image/tiff";
    if(iequals(ext, ".tif"))  return "image/tiff";
    if(iequals(ext, ".svg"))  return "image/svg+xml";
    if(iequals(ext, ".svgz")) return "image/svg+xml";
    return "application/text";
}

std::string api::path_cat(boost::beast::string_view base, boost::beast::string_view path)
{
    if(base.empty())
        return path.to_string();
    std::string result = base.to_string();
#if BOOST_MSVC
    char constexpr path_separator = '\\';
    if(result.back() == path_separator)
        result.resize(result.size() - 1);
    result.append(path.data(), path.size());
    for(auto& c : result)
        if(c == '/')
            c = path_separator;
#else
    char constexpr path_separator = '/';
    if(result.back() == path_separator)
        result.resize(result.size() - 1);
    result.append(path.data(), path.size());
#endif
    return result;
}

listener::listener(boost::asio::io_context& ioc, tcp::endpoint endpoint, std::shared_ptr<std::string const> const& doc_root)
        : m_ioc(ioc)
        , acceptor(boost::asio::make_strand(ioc))
        , doc_root(doc_root)
    {
         boost::beast::error_code ec;

        // Open the acceptor
        acceptor.open(endpoint.protocol(), ec);
        if(ec)
        {
	    std::cout<< "open : "<< ec.message()<<std::endl;
            return;
        }

	acceptor.set_option(boost::asio::socket_base::reuse_address(true));
    	if(ec)
    	{
        	std::cout<< "set options : " << ec.message() <<std::endl;
        	return;
    	}


	acceptor.bind(endpoint, ec);
        if(ec)
        {
            std::cout<<"bind : "<<ec.message()<<std::endl;;
            return;
        }
        // Start listening for connections
        acceptor.listen(boost::asio::socket_base::max_listen_connections, ec);
        if(ec)
        {
	    std::cout<< "listen : "<< ec.message()<<std::endl;
            return;
        }
	log.info("API started ! ");
}

void listener::run()
{
        if(! acceptor.is_open())
            return;
        do_accept();
}


void listener::do_accept()
{
      /*  acceptor.async_accept([this](boost::system::error_code ec, tcp::socket socket)
          	{
			if(!ec)
			{
				do_accept();
				std::make_shared<api>(
                std::move(socket),
                doc_root)->run();
			}
			else
				std::cout<<"accept : "<<ec.message()<<std::endl;
		});*/
		  acceptor.async_accept(
            boost::asio::make_strand(m_ioc),
            boost::beast::bind_front_handler(
                &listener::on_accept,
                this));
}

void listener::on_accept(boost::beast::error_code ec, tcp::socket socket)
{
        if(ec)
        {
            std::cout<<"accept : "<< ec.message() << std::endl;
        }
        else
        {
            // Create the session and run it
            std::make_shared<api>(
                std::move(socket),
                doc_root)->run();
        }

        // Accept another connection
        do_accept();
}
