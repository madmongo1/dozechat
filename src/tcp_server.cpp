#include "tcp_server.h"

tcp_server::tcp_server(boost::asio::io_context& ioc, tcp::endpoint& endpoint)
        : acceptor(net::make_strand(ioc))
        , socket(net::make_strand(ioc))
	, ioc(ioc)
{
	log.info("Starting server");
	//do_accept();
	beast::error_code ec;

        // Open the acceptor
        acceptor.open(endpoint.protocol(), ec);
        if(ec)
        {
            log.fail(ec, "open");
            return;
        }

        // Allow address reuse
        acceptor.set_option(net::socket_base::reuse_address(true), ec);
        if(ec)
        {
            log.fail(ec, "set_option");
            return;
        }

        // Bind to the server address
        acceptor.bind(endpoint, ec);
        if(ec)
        {
            log.fail(ec, "bind");
            return;
        }

        // Start listening for connections
        acceptor.listen(
            net::socket_base::max_listen_connections, ec);
        if(ec)
        {
            log.fail(ec, "listen");
            return;
        }
}



void tcp_server::do_accept()
{
	acceptor.async_accept(
	[this](boost::system::error_code ec, tcp::socket socket)
        {
          if (!ec)
          {
	    log.info("New client");
            std::make_shared<tcp_session>(std::move(socket), room)->start();
          }
          do_accept();
	});
}

void tcp_server::run()
{
	loop();
}
#include <boost/asio/yield.hpp>
void tcp_server::loop(beast::error_code ec)
{
	reenter(*this)
        {
            for(;;)
            {
                yield acceptor.async_accept(
                    socket,
                    std::bind(
                        &tcp_server::loop,
                        shared_from_this(),
                        std::placeholders::_1));
                if(ec)
                {
                    log.fail(ec, "accept");
                }
                else
              {
                    // Create the session and run it
                    std::make_shared<websocket_session>(std::move(socket), room)->run();
               }

                // Make sure each session gets its own strand
                socket = tcp::socket(net::make_strand(ioc));
            }
        }
}

#include <boost/asio/unyield.hpp>
